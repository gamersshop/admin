import { CModal, CModalBody, CModalHeader, CModalTitle, CButton, CModalFooter } from '@coreui/react'
import React, { useState } from 'react'
import propTypes from 'prop-types'
import { Stack, TextField } from '@mui/material'
import { toast } from 'react-toastify'

const EditCategory = ({ visibleEdit, setVisibleEdit, refreshPage, setRefreshPage, categorySelected, setCategorySelected }) => {
  EditCategory.propTypes = {
    visibleEdit: propTypes.bool,
    setVisibleEdit: propTypes.func,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
    categorySelected: propTypes.object,
    setCategorySelected: propTypes.func
  }

  const handleNewCategory = () => {
    if (validatedData()) {
      fetch(`http://localhost:8000/categories/${categorySelected._id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(categorySelected),
      })
        .then((response) => response.json())
        .then((data) => {
          toast.success('Your category has been updated!', {
            position: 'top-center',
            autoClose: 500,
          })
          console.log(data)
          setVisibleEdit(false)
          setRefreshPage(refreshPage + 1)
        })
        .catch((error) => {
          console.error('Error:', error.message)
        })
    }
  }

  //validation data
  const validatedData = () => {
    if (categorySelected.name === '') {
      toast.error('Your category name must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (categorySelected.description === '') {
      toast.error('Your category description must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    return true
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visibleEdit}
        onClose={() => setVisibleEdit(false)}
      >
        <CModalHeader onClose={() => setVisibleEdit(false)}>
          <CModalTitle>Update Category</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Stack spacing={2}>
            <div>
              <TextField
                id="name"
                value={categorySelected.name}
                variant="filled"
                label="Name"
                fullWidth
                onChange={(e) => setCategorySelected({ ...categorySelected, name: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="description"
                value={categorySelected.description}
                multiline
                rows={3}
                variant="filled"
                label="Description"
                fullWidth
                onChange={(e) => setCategorySelected({ ...categorySelected, description: e.target.value })}
              />
            </div>
          </Stack>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleEdit(false)}>
            Cancel
          </CButton>
          <CButton color="primary" onClick={handleNewCategory}>
            Save changes
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default EditCategory
