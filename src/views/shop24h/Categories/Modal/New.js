import { CModal, CModalBody, CModalHeader, CModalTitle, CButton, CModalFooter } from '@coreui/react'
import React, { useState } from 'react'
import propTypes from 'prop-types'
import { Stack, TextField } from '@mui/material'
import { toast } from 'react-toastify'

const NewCategory = ({ visible, setVisible, refreshPage, setRefreshPage }) => {
  NewCategory.propTypes = {
    visible: propTypes.bool,
    setVisible: propTypes.func,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
  }

  const [newCategory, setNewCategory] = useState({
    name: '',
    description: '',
  })

  const handleNewCategory = () => {
    if (validatedData()) {
      fetch(`http://localhost:8000/categories/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newCategory),
      })
        .then((response) => response.json())
        .then((data) => {
          toast.success('Your category has been created!', {
            position: 'top-center',
            autoClose: 500,
          })
          console.log(data)
          setVisible(false)
          setNewCategory({
            name: '',
            imageUrl: '',
          })
          setRefreshPage(refreshPage + 1)
        })
        .catch((error) => {
          console.error('Error:', error.message)
        })
    }
  }

  //validation data
  const validatedData = () => {
    if (newCategory.name === '') {
      toast.error('Your category name must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newCategory.description === '') {
      toast.error('Your category description must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    return true
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visible}
        onClose={() => setVisible(false)}
      >
        <CModalHeader onClose={() => setVisible(false)}>
          <CModalTitle>New Category</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Stack spacing={2}>
            <div>
              <TextField
                id="name"
                value={newCategory.name}
                variant="filled"
                label="Name"
                fullWidth
                onChange={(e) => setNewCategory({ ...newCategory, name: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="description"
                value={newCategory.description}
                multiline
                rows={3}
                variant="filled"
                label="Description"
                fullWidth
                onChange={(e) => setNewCategory({ ...newCategory, description: e.target.value })}
              />
            </div>
          </Stack>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisible(false)}>
            Cancel
          </CButton>
          <CButton color="success" onClick={handleNewCategory}>
            Save changes
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default NewCategory
