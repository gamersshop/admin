import React from 'react'
import {
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
  CRow,
  CCol,
  CCard,
  CCardHeader,
  CCardBody,
} from '@coreui/react'
import { IconButton } from '@mui/material'
import { AddCircleRounded } from '@mui/icons-material'
import { useEffect, useState } from 'react'
import NewCategory from './Modal/New'
import EditCategory from './Modal/Edit'
import DelCategory from './Modal/Del'

function Category() {
  const fetchAPI = async (url, body) => {
    let response = await fetch(url, body)
    let data = await response.json()
    return data
  }

  const [refreshPage, setRefreshPage] = useState(0)
  const [category, setCategory] = useState([])
  useEffect(() => {
    fetchAPI('http://localhost:8000/categories')
      .then((data) => {
        setCategory(data)
        console.log(data)
      })
      .catch((error) => {
        console.log(error)
      })
  }, [refreshPage])

  //add new
  const [visible, setVisible] = useState(false)
  const handleAddNewCategoryBtn = () => {
    setVisible(true)
  }

  //update
  const [visibleEdit, setVisibleEdit] = useState(false)
  const [categorySelected, setCategorySelected] = useState({})
  const handleEditCategoryBtn = (type) => {
    setVisibleEdit(true)
    setCategorySelected(type)
  }

  //del
  const [visibleDel, setVisibleDel] = useState(false)
  const handleDelCategoryBtn = (type) => {
    setVisibleDel(true)
    setCategorySelected(type)
  }

  return (
    <>
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>
              <strong>Category</strong>
            </CCardHeader>
            <CCardBody>
              <div className="col-2">
                <IconButton
                  color="success"
                  aria-label="add new category"
                  size="small"
                  onClick={handleAddNewCategoryBtn}
                >
                  <AddCircleRounded />
                  New Category
                </IconButton>
              </div>
              <CTable color="light" striped hover responsive>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">ID</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Description</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {category.map((type, index) => {
                    return (
                      <CTableRow key={type._id}>
                        <CTableDataCell>{type._id}</CTableDataCell>
                        <CTableDataCell>{type.name}</CTableDataCell>
                        <CTableDataCell>{type.description}</CTableDataCell>
                        <CTableDataCell className="col">
                          <button
                            className="btn btn-primary mb-2"
                            style={{ width: '80px' }}
                            onClick={() => handleEditCategoryBtn(type)}
                          >
                            Edit
                          </button>
                          <button
                            className="btn btn-danger"
                            style={{ width: '80px' }}
                            onClick={() => handleDelCategoryBtn(type)}
                          >
                            Del
                          </button>
                        </CTableDataCell>
                      </CTableRow>
                    )
                  })}
                </CTableBody>
              </CTable>
            </CCardBody>
          </CCard>
          {/* Modal */}
          <NewCategory
            visible={visible}
            setVisible={setVisible}
            refreshPage={refreshPage}
            setRefreshPage={setRefreshPage}
          />
          <EditCategory
            visibleEdit={visibleEdit}
            setVisibleEdit={setVisibleEdit}
            refreshPage={refreshPage}
            setRefreshPage={setRefreshPage}
            categorySelected={categorySelected} 
            setCategorySelected={setCategorySelected}    
          />
          <DelCategory
            visibleDel={visibleDel}
            setVisibleDel={setVisibleDel}
            refreshPage={refreshPage}
            setRefreshPage={setRefreshPage}
            categorySelected={categorySelected} 
          />
        </CCol>
      </CRow>
    </>
  )
}

export default Category
