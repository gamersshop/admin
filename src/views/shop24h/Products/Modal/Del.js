import { CModal, CModalBody, CModalHeader, CModalTitle, CButton, CModalFooter } from '@coreui/react'
import React from 'react'
import propTypes from 'prop-types'
import { toast } from 'react-toastify'

const DelProduct = ({
  visibleDel,
  setVisibleDel,
  productSelected,
  refreshPage,
  setRefreshPage,
}) => {
  DelProduct.propTypes = {
    visibleDel: propTypes.bool,
    setVisibleDel: propTypes.func,
    productSelected: propTypes.object,
    setProductSelected: propTypes.func,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
  }

  const handleDelProduct = () => {
    console.log(productSelected)
    fetch(`http://localhost:8000/products/${productSelected._id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((data) => {
        toast.warning('Your product has been deleted!', {
          position: 'top-center',
          autoClose: 500,
        })
        setVisibleDel(false)
        setRefreshPage(refreshPage + 1)
      })
      .catch((error) => {
        console.error('Error:', error.message)
      })
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visibleDel}
        onClose={() => setVisibleDel(false)}
      >
        <CModalHeader color="danger" onClose={() => setVisibleDel(false)}>
          <CModalTitle>Delete Confirmation</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <h5 className="text-center text-danger">Are you sure to DELETE <strong>{productSelected?.name}</strong>???</h5>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleDel(false)}>
            Cancel
          </CButton>
          <CButton color="danger" onClick={handleDelProduct}>
            Confirm
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default DelProduct
