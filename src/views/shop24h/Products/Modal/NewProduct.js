import { CModal, CModalBody, CModalHeader, CModalTitle, CButton, CModalFooter } from '@coreui/react'
import React, { useState } from 'react'
import propTypes from 'prop-types'
import { Stack, TextField, FormControl, InputLabel, Select, MenuItem, Input } from '@mui/material'
import { toast } from 'react-toastify'
import { storage } from '../../../../firebase'
import { ref, uploadBytes } from 'firebase/storage'

const NewProduct = ({ visibleNew, setVisibleNew, refreshPage, setRefreshPage }) => {
  NewProduct.propTypes = {
    visibleNew: propTypes.bool,
    setVisibleNew: propTypes.func,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
  }

  const [newProduct, setNewProduct] = useState({
    name: '',
    imageUrl: '',
    description: '',
    buyPrice: 0,
    promotionPrice: 0,
    amount: 50,
    category: '',
    platform: '',
  })

  const [imageUpload, setImageUpload] = useState(null)
  
  const handleUploadImage = () => {
    if(imageUpload === null) return;
    const imgRef = ref(storage, `images/${imageUpload.name}`);
    uploadBytes(imgRef, imageUpload).then(() => toast.success("Upload Img Successfully!", { position: "top-center", autoClose: 500}))
  }

  const handleNewProduct = () => {
    if (validatedData()) {
      fetch(`http://localhost:8000/products/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newProduct),
      })
        .then((response) => response.json())
        .then((data) => {
          toast.success('Your product has been created!', {
            position: 'top-center',
            autoClose: 500,
          })
          console.log(data)
          setVisibleNew(false)
          setNewProduct({
            name: '',
            imageUrl: '',
            description: '',
            buyPrice: 0,
            promotionPrice: 0,
            amount: 50,
            category: '',
            platform: '',
          })
          setRefreshPage(refreshPage + 1)
        })
        .catch((error) => {
          console.error('Error:', error.message)
        })
    }
  }

  //validation data
  const validatedData = () => {
    if (newProduct.name === '') {
      toast.error('Your product name must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newProduct.imageUrl === '') {
      toast.error('Your product imageUrl must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newProduct.description === '') {
      toast.error('Your product description must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newProduct.category === '') {
      toast.error('Your product category must be selected!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newProduct.platform === '') {
      toast.error('Your product platform must be selected!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newProduct.buyPrice === '') {
      toast.error('Your product [buyPrice] must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (Number(newProduct.buyPrice) <= Number(newProduct.promotionPrice)) {
      toast.error('[buyPrice]  must be greater than [promotionPrice]!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    return true
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visibleNew}
        onClose={() => setVisibleNew(false)}
      >
        <CModalHeader onClose={() => setVisibleNew(false)}>
          <CModalTitle>New Product</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Stack spacing={2}>
            <div>
              <TextField
                id="name"
                value={newProduct.name}
                variant="filled"
                label="Name"
                fullWidth
                onChange={(e) => setNewProduct({ ...newProduct, name: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="imageUrl"
                variant="filled"
                label="ImgUrl"
                fullWidth
                value={newProduct.imageUrl}
                onChange={(e) => setNewProduct({ ...newProduct, imageUrl: e.target.value })}
              />
            </div>
            <div className="row">
              <div className="col-4">
                <InputLabel>Upload an Image</InputLabel>
              </div>
              <div className="col-4">
                <Input type="file" fullWidth onChange={(e) => setImageUpload(e.target.files[0])} />
              </div>
              <CButton onClick={handleUploadImage} className="col-3">
                Click to upload
              </CButton>
            </div>
            <div>
              <TextField
                id="description"
                value={newProduct.description}
                multiline
                rows={3}
                variant="filled"
                label="Description"
                fullWidth
                onChange={(e) => setNewProduct({ ...newProduct, description: e.target.value })}
              />
            </div>
            <Stack direction={'row'} spacing={1}>
              <FormControl variant="filled" className="col-sm-12 col-md-6">
                <InputLabel id="categoryLabel">Category</InputLabel>
                <Select
                  labelId="categoryLabel"
                  id="category"
                  value={newProduct.category}
                  label="Category"
                  onChange={(e) => setNewProduct({ ...newProduct, category: e.target.value })}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={'62d981c670db2be556175020'}>Console Gaming</MenuItem>
                  <MenuItem value={'62da13097c837a7d3ca28d4a'}>Video Games</MenuItem>
                  <MenuItem value={'62da12dd7c837a7d3ca28d48'}>Accessories</MenuItem>
                </Select>
              </FormControl>
              <FormControl variant="filled" className="col-sm-12 col-md-6">
                <InputLabel id="platformLabel">Platform</InputLabel>
                <Select
                  labelId="platformLabel"
                  id="platform"
                  value={newProduct.platform}
                  label="Platform"
                  onChange={(e) => setNewProduct({ ...newProduct, platform: e.target.value })}
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={'62da1596b66ac0acd95d4240'}>PC</MenuItem>
                  <MenuItem value={'62da1539b66ac0acd95d423c'}>Playstation</MenuItem>
                  <MenuItem value={'62da150db66ac0acd95d423a'}>Xbox</MenuItem>
                  <MenuItem value={'62da1566b66ac0acd95d423e'}>Nintendo</MenuItem>
                </Select>
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={2}>
              <TextField
                id="buyPrice"
                value={newProduct.buyPrice}
                variant="filled"
                label="Price"
                onChange={(e) => setNewProduct({ ...newProduct, buyPrice: e.target.value })}
              />
              <TextField
                value={newProduct.promotionPrice}
                variant="filled"
                label="Discount"
                onChange={(e) => setNewProduct({ ...newProduct, promotionPrice: e.target.value })}
              />
              <TextField
                value={newProduct.amount}
                variant="filled"
                label="Amount"
                onChange={(e) => setNewProduct({ ...newProduct, amount: e.target.value })}
              />
            </Stack>
          </Stack>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleNew(false)}>
            Cancel
          </CButton>
          <CButton color="success" onClick={handleNewProduct}>
            Save changes
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default NewProduct
