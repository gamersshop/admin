import { CModal, CModalBody, CModalHeader, CModalTitle, CButton, CModalFooter } from '@coreui/react'
import React from 'react'
import propTypes from 'prop-types'
import { Stack, TextField, FormControl, InputLabel, Select, MenuItem, Input } from '@mui/material'
import { toast } from 'react-toastify'

const EditProduct = ({ visible, setVisible, productSelected, setProductSelected, refreshPage, setRefreshPage }) => {
  EditProduct.propTypes = {
    visible: propTypes.bool,
    setVisible: propTypes.func,
    productSelected: propTypes.object,
    setProductSelected: propTypes.func,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
  }

  const handleUpdateProduct = () => {
    console.log(productSelected)
    if (validatedData()) {
      fetch(`http://localhost:8000/products/${productSelected._id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(productSelected),
      })
        .then((response) => response.json())
        .then((data) => {
          toast.success(`${productSelected.name} has been updated!`, {
            position: 'top-center',
            autoClose: 500,
          })
          setVisible(false)
          setRefreshPage(refreshPage + 1)
        })
        .catch((error) => {
          console.error('Error:', error.message)
        })
    }
  }

  //validation data
  const validatedData = () => {
    if (productSelected.name === '') {
      toast.error('Your product name must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (productSelected.imageUrl === '') {
      toast.error('Your product imageUrl must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (productSelected.description === '') {
      toast.error('Your product description must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (productSelected.buyPrice <= productSelected.promotionPrice) {
      toast.error('[buyPrice]  must be greater than [promotionPrice]!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (productSelected.amount < 0) {
      toast.error('Your product rate must be larger than 0!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }
    return true
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visible}
        onClose={() => setVisible(false)}
      >
        <CModalHeader onClose={() => setVisible(false)}>
          <CModalTitle>Product Information</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Stack spacing={2}>
            <div>
              <TextField
                id="name"
                value={productSelected?.name}
                variant="filled"
                label="Name"
                fullWidth
                onChange={(e) => setProductSelected({ ...productSelected, name: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="imageUrl"
                variant="filled"
                label="ImgUrl"
                fullWidth
                value={productSelected?.imageUrl}
                onChange={(e) =>
                  setProductSelected({ ...productSelected, imageUrl: e.target.value })
                }
              />
            </div>
            <div className='row'>
              <div className='col-4'>
                <InputLabel>
                  Upload an Image
                </InputLabel>
              </div>
              <div className='col-8'>
                <Input 
                  type='file'
                  fullWidth
                />
              </div>
            </div>
            <div>
              <TextField
                id="description"
                value={productSelected?.description}
                multiline
                rows={3}
                variant="filled"
                label="Description"
                fullWidth
                onChange={(e) =>
                  setProductSelected({ ...productSelected, description: e.target.value })
                }
              />
            </div>
            <Stack direction={'row'} spacing={1}>
              <FormControl variant="filled" className="col-sm-12 col-md-6">
                <InputLabel id="categoryLabel">Category</InputLabel>
                <Select
                  labelId="categoryLabel"
                  id="category"
                  value={productSelected?.category._id}
                  label="Category"
                  onChange={(e) =>
                    setProductSelected({ ...productSelected, category: { _id: e.target.value } })
                  }
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={'62d981c670db2be556175020'}>Console Gaming</MenuItem>
                  <MenuItem value={'62da13097c837a7d3ca28d4a'}>Video Games</MenuItem>
                  <MenuItem value={'62da12dd7c837a7d3ca28d48'}>Accessories</MenuItem>
                </Select>
              </FormControl>
              <FormControl variant="filled" className="col-sm-12 col-md-6">
                <InputLabel id="platformLabel">Platform</InputLabel>
                <Select
                  labelId="platformLabel"
                  id="platform"
                  value={productSelected?.platform._id}
                  label="Platform"
                  onChange={(e) =>
                    setProductSelected({ ...productSelected, platform: { _id: e.target.value } })
                  }
                >
                  <MenuItem value="">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={'62da1596b66ac0acd95d4240'}>PC</MenuItem>
                  <MenuItem value={'62da1539b66ac0acd95d423c'}>Playstation</MenuItem>
                  <MenuItem value={'62da150db66ac0acd95d423a'}>Xbox</MenuItem>
                  <MenuItem value={'62da1566b66ac0acd95d423e'}>Nintendo</MenuItem>
                </Select>
              </FormControl>
            </Stack>
            <Stack direction="row" spacing={2}>
              <TextField
                id="buyPrice"
                value={productSelected?.buyPrice}
                variant="filled"
                label="Price"
                onChange={(e) =>
                  setProductSelected({ ...productSelected, buyPrice: e.target.value })
                }
              />
              <TextField
                id="promotionPrice"
                value={productSelected?.promotionPrice}
                variant="filled"
                label="Discount"
                onChange={(e) =>
                  setProductSelected({ ...productSelected, promotionPrice: e.target.value })
                }
              />
              <TextField
                id="rate"
                value={productSelected?.amount}
                variant="filled"
                label="Amount"
                onChange={(e) =>
                  setProductSelected({ ...productSelected, amount: (e.target.value) })
                }
              />
            </Stack>
          </Stack>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisible(false)}>
            Cancel
          </CButton>
          <CButton color="primary" onClick={handleUpdateProduct}>
            Save changes
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default EditProduct
