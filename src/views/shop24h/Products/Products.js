import React from 'react'
import {
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
  CRow,
  CCol,
  CCard,
  CCardHeader,
  CCardBody,
  CFormInput,
  CButton,
  CImage,
} from '@coreui/react'
import { useEffect, useState } from 'react'
import { Stack, Pagination, IconButton } from '@mui/material'
import { AddCircleRounded } from '@mui/icons-material'
import EditProduct from './Modal/Edit'
import NewProduct from './Modal/NewProduct'
import DelProduct from './Modal/Del'

function Product() {
  const [products, setProducts] = useState([])
  const [noPage, setNoPage] = useState(5)
  const [pageIndex, setPageIndex] = useState(1)
  const [refreshPage, setRefreshPage] = useState(0)
  const [searchInfo, setSearchInfo] = useState('')

  const getAllProducts = async () => {
    fetch('http://localhost:8000/products')
      .then((res) => res.json())
      .then((data) => {
        setNoPage(Math.ceil(data.length / 8))
        setProducts(data.slice((pageIndex - 1) * 8, pageIndex * 8))
      })
      .catch((error) => {
        console.log(error)
      })
  }

  useEffect(() => {
    window.scrollTo(5, 5)
    getAllProducts()
  }, [noPage, pageIndex, refreshPage])

  const onPageIndexChange = (event, value) => {
    setPageIndex(value)
  }

  //filter
  const handleSearchByName = (event) => {
    if (event.key === 'Enter') {
      const findProduct = async () => {
        fetch('http://localhost:8000/products')
          .then((res) => res.json())
          .then((data) => {
            const updateProductList = data.filter((p) => p.name.toLowerCase().includes(searchInfo))
            setProducts(updateProductList)
          })
          .catch((error) => {
            console.log(error)
          })
      }
      findProduct()
    }

  }

  //clear filter
  const handleClearBtn = () => {
    setSearchInfo('')
    getAllProducts()
  }

  //add new
  const [visibleNew, setVisibleNew] = useState(false)
  const handleAddNewProductBtn = () => {
    setVisibleNew(true)
  }

  //update
  const [visible, setVisible] = useState(false)
  const [productSelected, setProductSelected] = useState(null)
  const handleEditBtn = (product) => {
    setVisible(true)
    setProductSelected(product)
  }

  //del
  const [visibleDel, setVisibleDel] = useState(false)
  const handleDelBtn = (product) => {
    setVisibleDel(true)
    setProductSelected(product)
  }

  return (
    <>
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>
              <strong>Product</strong>
            </CCardHeader>
            <CCardBody>
              <div className="d-flex justify-content-between mb-2">
                <div className="col-2">
                  <IconButton
                    color="success"
                    aria-label="add new product"
                    size="small"
                    onClick={handleAddNewProductBtn}
                  >
                    <AddCircleRounded />
                    New Product
                  </IconButton>
                </div>
                <div className="row col-4">
                  <div className="col-8">
                    <CFormInput
                      placeholder="search by name.."
                      value={searchInfo}
                      onChange={(e) => setSearchInfo(e.target.value)}
                      onKeyDown={handleSearchByName}
                    ></CFormInput>
                  </div>
                  <div className="col-4" style={{ marginLeft: '-18px' }}>
                    <CButton color="danger" onClick={handleClearBtn}>
                      Clear
                    </CButton>
                  </div>
                </div>
              </div>
              <CTable hover bordered responsive>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Img</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Category</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Platform</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Buy Price</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Promotion Price</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Amount</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {products.map((product, index) => {
                    return (
                      <CTableRow key={product._id}>
                        <CTableHeaderCell>{product.name}</CTableHeaderCell>
                        <CTableDataCell>
                          <CImage src={product.imageUrl} width="80%" height={80} />
                        </CTableDataCell>
                        <CTableDataCell>{product.category.name}</CTableDataCell>
                        <CTableDataCell>{product.platform.name}</CTableDataCell>
                        <CTableDataCell>{product.buyPrice}$</CTableDataCell>
                        <CTableDataCell>{product.promotionPrice}$</CTableDataCell>
                        <CTableDataCell>{product.amount}</CTableDataCell>
                        <CTableDataCell className="row">
                          <button
                            className="btn btn-primary"
                            style={{ width: '60px' }}
                            onClick={() => handleEditBtn(product)}
                          >
                            Edit
                          </button>
                          <button
                            className="btn btn-danger"
                            style={{ width: '60px' }}
                            onClick={() => handleDelBtn(product)}
                          >
                            Del
                          </button>
                        </CTableDataCell>
                      </CTableRow>
                    )
                  })}
                </CTableBody>
              </CTable>
              <Stack spacing={2} mt={2} mb={1} direction="row" justifyContent={'flex-end'}>
                <Pagination
                  variant="outlined"
                  color="secondary"
                  defaultPage={1}
                  count={noPage}
                  onChange={onPageIndexChange}
                />
              </Stack>
            </CCardBody>
          </CCard>
          {/* Modal */}
          <NewProduct
            visibleNew={visibleNew}
            setVisibleNew={setVisibleNew}
            refreshPage={refreshPage}
            setRefreshPage={setRefreshPage}
          />
          <EditProduct
            visible={visible}
            setVisible={setVisible}
            productSelected={productSelected}
            setProductSelected={setProductSelected}
            refreshPage={refreshPage}
            setRefreshPage={setRefreshPage}
          />
          <DelProduct
            visibleDel={visibleDel}
            setVisibleDel={setVisibleDel}
            productSelected={productSelected}
            refreshPage={refreshPage}
            setRefreshPage={setRefreshPage}
          />
        </CCol>
      </CRow>
    </>
  )
}

export default Product
