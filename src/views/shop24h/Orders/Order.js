import React, { useState, useEffect } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CFormInput,
  CButton,
} from '@coreui/react'
import { Stack, Pagination, IconButton } from '@mui/material'
import { AddCircleRounded } from '@mui/icons-material'
import NewOrder from './Modal/New'
import DelOrder from './Modal/Del'
import EditOrder from './Modal/Edit'

const Orders = () => {
  const [orders, setOrders] = useState([])
  const [noPage, setNoPage] = useState(5)
  const [pageIndex, setPageIndex] = useState(1)
  const [refreshPage, setRefreshPage] = useState(0)
  const [searchInfo, setSearchInfo] = useState('')

  const onPageIndexChange = (event, value) => {
    setPageIndex(value)
  }

  const getAllOrders = async () => {
    fetch('http://localhost:8000/orders')
      .then((res) => res.json())
      .then((data) => {
        console.log(data)
        setNoPage(Math.ceil(data.length / 10))
        setOrders(data.slice((pageIndex - 1) * 10, pageIndex * 10))
      })
      .catch((error) => {
        console.error(error.message)
      })
  }

  useEffect(() => {
    window.scrollTo(5, 5)
    getAllOrders()
  }, [refreshPage, noPage, pageIndex])

  //filter
  const handleSearchByID = (event) => {
    if (event.key === 'Enter') {
      const findOrder = async () => {
        fetch('http://localhost:8000/orders')
          .then((res) => res.json())
          .then((data) => {
            console.log(searchInfo)
            const updateOrderList = data.filter((p) => p._id === searchInfo)
            console.log(updateOrderList)
            setOrders(updateOrderList)
          })
          .catch((error) => {
            console.log(error)
          })
      }
      findOrder()
    }
  }

  //clear filter
  const handleClearBtn = () => {
    setSearchInfo('')
    getAllOrders()
  }

  //new
  const [visibleNew, setVisibleNew] = useState(false)
  const handleAddNewOrder = () => {
    setVisibleNew(true)
  }

  //update
  const [visibleEdit, setVisibleEdit] = useState(false)
  const [orderSelected, setOrderSelected] = useState({})
  const handleEditBtn = (order) => {
    setVisibleEdit(true)
    setOrderSelected(order)
  }

  //del
  const [visibleDel, setVisibleDel] = useState(false)
  const handleDelBtn = (order) => {
    setVisibleDel(true)
    setOrderSelected(order)
  }

  return (
    <CRow>
      <CCol>
        <CCard>
          <CCardHeader>
            <strong>Orders</strong>
          </CCardHeader>
          <CCardBody>
            <div className="d-flex justify-content-between mb-2">
              <div className="col-2">
                <IconButton
                  color="success"
                  aria-label="add new order"
                  size="small"
                  onClick={handleAddNewOrder}
                >
                  <AddCircleRounded />
                  New Order
                </IconButton>
              </div>
              <div className="row col-4">
                <div className="col-8">
                  <CFormInput
                    placeholder="search by ID.."
                    value={searchInfo}
                    onChange={(e) => setSearchInfo(e.target.value)}
                    onKeyDown={handleSearchByID}
                  ></CFormInput>
                </div>
                <div className="col-4" style={{ marginLeft: '-18px' }}>
                  <CButton color="danger" onClick={handleClearBtn}>
                    Clear
                  </CButton>
                </div>
              </div>
            </div>

            <CTable border={1} striped hover responsive>
              <CTableHead>
                <CTableRow>
                  <CTableHeaderCell scope="col">ID</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Receiver</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Address</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Order Date</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Shipped Date</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Order Detail</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Cost</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Note</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                </CTableRow>
              </CTableHead>
              <CTableBody>
                {orders.map((order, index) => {
                  return (
                    <CTableRow key={index}>
                      <CTableHeaderCell>{order._id}</CTableHeaderCell>
                      <CTableDataCell>{order.receiver}</CTableDataCell>
                      <CTableDataCell>{order.address}</CTableDataCell>
                      <CTableDataCell>{order.orderDate.slice(0, 10)}</CTableDataCell>
                      <CTableDataCell>{order.shippedDate.slice(0, 10)}</CTableDataCell>
                      <CTableDataCell>
                        {order.orderDetail.map((product,index) => (
                          <p key={index}>
                            {product.name} x{product.amount}
                          </p>
                        ))}
                      </CTableDataCell>
                      <CTableDataCell>{order.cost} $</CTableDataCell>
                      <CTableDataCell>{order.note}</CTableDataCell>
                      <CTableDataCell
                        className="row"
                        style={{
                          padding: '39px 0',
                          justifyContent: 'center',
                        }}
                      >
                        <button
                          className="btn btn-primary mx-1"
                          style={{ width: '80px' }}
                          onClick={() => handleEditBtn(order)}
                        >
                          Edit
                        </button>
                        <button
                          className="btn btn-danger"
                          style={{ width: '80px' }}
                          onClick={() => handleDelBtn(order)}
                        >
                          Del
                        </button>
                      </CTableDataCell>
                    </CTableRow>
                  )
                })}
              </CTableBody>
            </CTable>
            <Stack spacing={2} mt={2} mb={1} direction="row" justifyContent={'flex-end'}>
              <Pagination
                variant="outlined"
                color="primary"
                defaultPage={1}
                count={noPage}
                onChange={onPageIndexChange}
              />
            </Stack>
          </CCardBody>
        </CCard>
        {/* Modal */}
        <NewOrder
          visibleNew={visibleNew}
          setVisibleNew={setVisibleNew}
          refreshPage={refreshPage}
          setRefreshPage={setRefreshPage}
        />
        <EditOrder
          visibleEdit={visibleEdit}
          setVisibleEdit={setVisibleEdit}
          refreshPage={refreshPage}
          setRefreshPage={setRefreshPage}
          orderSelected={orderSelected}
          setOrderSelected={setOrderSelected}
        />
        <DelOrder
          visibleDel={visibleDel}
          setVisibleDel={setVisibleDel}
          refreshPage={refreshPage}
          setRefreshPage={setRefreshPage}
          orderSelected={orderSelected}
        />
      </CCol>
    </CRow>
  )
}

export default Orders
