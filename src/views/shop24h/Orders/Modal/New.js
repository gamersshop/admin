import {
  CModal,
  CModalBody,
  CButton,
  CModalFooter,
  CCol,
  CRow,
  CFormSelect,
  CFormInput,
  CFormLabel,
  CFormTextarea,
} from '@coreui/react'
import React, { useEffect, useState } from 'react'
import propTypes from 'prop-types'
import { Stack } from '@mui/material'
import { toast } from 'react-toastify'
import CIcon from '@coreui/icons-react'
import { cilTrash } from '@coreui/icons'

const NewOrder = ({ visibleNew, setVisibleNew, refreshPage, setRefreshPage }) => {
  NewOrder.propTypes = {
    visibleNew: propTypes.bool,
    setVisibleNew: propTypes.func,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
  }

  const [products, setProducts] = useState([])
  const [count, setCount] = useState(0)

  useEffect(() => {
    const getAllProducts = async () => {
      fetch('http://localhost:8000/products')
        .then((res) => res.json())
        .then((data) => setProducts(data))
        .catch((error) => {
          console.error(error.message)
        })
    }
    getAllProducts()
  }, [])

  const [newOrder, setNewOrder] = useState({
    orderDate: '',
    shippedDate: '',
    orderDetail: [],
    cost: 0,
    fullName: '',
    phone: '',
    email: '',
    address: '',
    city: '',
    country: '',
    note: '',
  })

  const onBtnAddNewProduct = () => {
    let newOrderDetail = {
      id: '',
      name: '',
      quantity: 1,
      price: 0,
    }
    setNewOrder({ ...newOrder, orderDetail: [...newOrder.orderDetail, newOrderDetail] })
    setCount(count + 1)
  }

  const changeSelectProduct = (event, index) => {
    const productId = event.target.value
    newOrder.orderDetail[index].id = productId
    newOrder.orderDetail[index].name = getName(productId)
    newOrder.orderDetail[index].price =
      getPrice(newOrder.orderDetail[index].id) * parseInt(newOrder.orderDetail[index].quantity)
    setNewOrder({ ...newOrder, orderDetail: [...newOrder.orderDetail] })
  }

  const getName = (paramId) => {
    let productSelected = []
    if (paramId) {
      productSelected = products.filter((product, index) => {
        return product._id === paramId
      })
    }
    return productSelected[0].name
  }

  const getPrice = (paramId) => {
    let productSelected = []
    if (paramId) {
      productSelected = products.filter((product, index) => {
        return product._id === paramId
      })
    }
    return productSelected[0].buyPrice - productSelected[0].promotionPrice
  }

  const changeQuantity = (event, index) => {
    if (event.target.value > 0 && newOrder.orderDetail[index].id !== '') {
      newOrder.orderDetail[index].quantity = event.target.value
      newOrder.orderDetail[index].price =
        getPrice(newOrder.orderDetail[index].id) * parseInt(newOrder.orderDetail[index].quantity)
      setNewOrder({ ...newOrder, orderDetail: [...newOrder.orderDetail] })
    }
  }

  //sum total price
  const sumTotal = (orderDetail) => {
    const sum = orderDetail.reduce((preValue, product) => {
      return preValue + product.price
    }, 0)
    return sum
  }

  const [customers, setCustomers] = useState([])
  const [customerNameSelected, setCustomerNameSelected] = useState('')
  const [customerSelected, setCustomerSelected] = useState({})

  useEffect(() => {
    fetch('http://localhost:8000/customers')
      .then((res) => res.json())
      .then((data) => {
        setCustomers(data)
      })
      .catch((error) => {
        console.error('Error:', error.message)
      })
  }, [customerSelected])

  const changeSelectCustomer = (event) => {
    setCustomerNameSelected(event.target.value)
    const customer = customers.find((c) => c._id === event.target.value)
    setCustomerSelected(customer)
  }

  useEffect(() => {
    setNewOrder({ ...newOrder, cost: sumTotal(newOrder.orderDetail) })
  }, [newOrder.orderDetail])

  useEffect(() => {
    if (customerSelected) {
      setNewOrder({
        ...newOrder,
        fullName: customerSelected.fullName,
        phone: customerSelected.phone,
        email: customerSelected.email,
        address: customerSelected.address,
        city: customerSelected.city,
        country: customerSelected.country,
      })
    }
  }, [customerSelected])

  useEffect(() => {
    if(count > 0) {
      const orderList = newOrder.orderDetail.map((p) => p.id)
      console.log(orderList)
      const updateProductList = products.filter((p) => p._id !== orderList.forEach((e) => e))
      console.log(updateProductList)
      setProducts(updateProductList)

    }
  },[count])

  const onBtnDeleteClick = (index) => {
    newOrder.orderDetail.splice(index, 1)
    setCount(count - 1)
    setNewOrder({ ...newOrder, orderDetail: [...newOrder.orderDetail] })
  }

  const handleNewOrder = () => {
    if (validatedData()) {
      fetch(`http://localhost:8000/customers/order`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newOrder),
      })
        .then((response) => response.json())
        .then((data) => {
          setVisibleNew(false)
          toast.success(`New Order ${data._id} has been created!`, {
            position: 'top-center',
            autoClose: 500,
          })
          console.log(data)
          setNewOrder({
            orderDate: '',
            shippedDate: '',
            orderDetail: [],
            cost: 0,
            fullName: '',
            phone: '',
            email: '',
            address: '',
            city: '',
            country: '',
            note: '',
          })
          setCustomerSelected({})
          setCount(0)
          setRefreshPage(refreshPage + 1)
        })
        .catch((error) => {
          console.error('Error:', error.message)
        })
    }
  }

  //validation data
  const validatedData = () => {
    if (newOrder.fullName === '') {
      toast.error('Your Order full name must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newOrder.phone === '') {
      toast.error('Your Order phone number must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newOrder.email === '') {
      toast.error('Your Order email must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    let regex = new RegExp(
      //eslint-disable-next-line
      "([!#-'*+/-9=?A-Z^-~-]+(.[!#-'*+/-9=?A-Z^-~-]+)*|\"([]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(.[!#-'*+/-9=?A-Z^-~-]+)*|[[\t -Z^-~]*])",
    )
    if (!regex.test(newOrder.email)) {
      toast.error(`Your email is invalid!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (!newOrder.address) {
      toast.error('Your Order address must be selected!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (!newOrder.orderDate) {
      toast.error('Your Order Date must be selected!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (!newOrder.shippedDate) {
      toast.error('Your Shipped Date must be selected!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (!newOrder.orderDetail) {
      toast.error('Your Product must be selected!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }
    return true
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visibleNew}
        onClose={() => setVisibleNew(false)}
      >
        <CModalBody>
          <h5 className="text-center">Customer Information</h5>
          <div className="col-sm-6 col-md-4">
            <CFormSelect
              value={customerNameSelected.id}
              onChange={(e) => {
                changeSelectCustomer(e)
              }}
            >
              <option value={1}>Select Existed Customer</option>
              {customers.map((customer, index) => {
                return (
                  <option key={index} value={customer._id}>
                    {customer.fullName}
                  </option>
                )
              })}
            </CFormSelect>
          </div>
          <Stack direction={'row'} spacing={1}>
            <div className="col-sm-12 col-md-6">
              <CRow className="mt-3">
                <CCol xs={4}>
                  <CFormLabel style={{ fontWeight: 'bolder' }}>Full Name</CFormLabel>
                </CCol>
                <CCol xs={8}>
                  <CFormInput
                    defaultValue={''}
                    value={customerSelected ? customerSelected?.fullName : newOrder.fullName}
                    onChange={(e) => setNewOrder({ ...newOrder, fullName: e.target.value })}
                  />
                </CCol>
              </CRow>
              <CRow className="mt-3">
                <CCol xs={4}>
                  <CFormLabel style={{ fontWeight: 'bolder' }}>Address</CFormLabel>
                </CCol>
                <CCol xs={8}>
                  <CFormInput
                    defaultValue={''}
                    value={customerSelected ? customerSelected?.address : newOrder.address}
                    onChange={(e) => setNewOrder({ ...newOrder, address: e.target.value })}
                  />
                </CCol>
              </CRow>
              <CRow className="mt-3">
                <CCol xs={4}>
                  <CFormLabel style={{ fontWeight: 'bolder' }}>City</CFormLabel>
                </CCol>
                <CCol xs={8}>
                  <CFormInput
                    defaultValue={''}
                    value={customerSelected ? customerSelected?.city : newOrder.city}
                    onChange={(e) => {
                      setNewOrder({ ...newOrder, city: e.target.value })
                    }}
                  />
                </CCol>
              </CRow>
              <CRow className="mt-3">
                <CCol xs={4}>
                  <CFormLabel style={{ fontWeight: 'bolder' }}>Country</CFormLabel>
                </CCol>
                <CCol xs={8}>
                  <CFormInput
                    defaultValue={''}
                    value={customerSelected ? customerSelected?.country : newOrder.country}
                    onChange={(e) => setNewOrder({ ...newOrder, country: e.target.value })}
                  />
                </CCol>
              </CRow>
            </div>
            <div className="col-sm-12 col-md-6">
              <CRow className="mt-3">
                <CCol xs={4}>
                  <CFormLabel style={{ fontWeight: 'bolder' }}>Phone</CFormLabel>
                </CCol>
                <CCol xs={8}>
                  <CFormInput
                    defaultValue={''}
                    value={customerSelected ? customerSelected?.phone : newOrder.phone}
                    onChange={(e) => setNewOrder({ ...newOrder, phone: e.target.value })}
                  />
                </CCol>
              </CRow>
              <CRow className="mt-3">
                <CCol xs={4}>
                  <CFormLabel style={{ fontWeight: 'bolder' }}>Email</CFormLabel>
                </CCol>
                <CCol xs={8}>
                  <CFormInput
                    defaultValue={''}
                    value={customerSelected ? customerSelected?.email : newOrder.email}
                    onChange={(e) => setNewOrder({ ...newOrder, email: e.target.value })}
                  />
                </CCol>
              </CRow>

              <CRow className="mt-3">
                <CCol xs={4}>
                  <CFormLabel style={{ fontWeight: 'bolder' }}>Order Date</CFormLabel>
                </CCol>
                <CCol xs={8}>
                  <CFormInput
                    value={newOrder.orderDate}
                    type={'date'}
                    onChange={(e) => setNewOrder({ ...newOrder, orderDate: e.target.value })}
                  />
                </CCol>
              </CRow>
              <CRow className="mt-3">
                <CCol xs={4}>
                  <CFormLabel style={{ fontWeight: 'bolder' }}>Shipped Date</CFormLabel>
                </CCol>
                <CCol xs={8}>
                  <CFormInput
                    value={newOrder.shippedDate}
                    type={'date'}
                    onChange={(e) => setNewOrder({ ...newOrder, shippedDate: e.target.value })}
                  />
                </CCol>
              </CRow>
            </div>
          </Stack>
          <CRow className="mt-3">
            <CCol xs={4}>
              <CFormLabel style={{ fontWeight: 'bolder' }}>Note</CFormLabel>
            </CCol>
            <CCol xs={8}>
              <CFormTextarea
                value={newOrder.note}
                rows={2}
                onChange={(e) => setNewOrder({ ...newOrder, note: e.target.value })}
              />
            </CCol>
          </CRow>
          <hr />
          <h5 className="text-center">Order Detail</h5>
          <Stack spacing={1}>
            <CRow>
              <CCol xs={8} className="text-left">
                <CButton onClick={onBtnAddNewProduct}>Add Product</CButton>
              </CCol>
              <CCol xs={4}>
                <CRow>
                  <CCol xs={4} className="text-right">
                    <CFormLabel style={{ fontWeight: 'bolder' }}>Total</CFormLabel>
                  </CCol>
                  <CCol xs={8}>
                    <CFormInput value={`${newOrder.cost}$`} readOnly />
                  </CCol>
                </CRow>
              </CCol>
            </CRow>

            {Array.from({ length: count }, (product, i) => {
              return (
                <CRow key={i} className="mt-3">
                  <CCol xs={7}>
                    <CFormSelect
                      value={newOrder.orderDetail[i]?.id}
                      onChange={(e) => {
                        changeSelectProduct(e, i)
                      }}
                    >
                      <option value={'1'}>Select Product</option>
                      {products.map((product, index) => {
                        return (
                          <option key={index} value={product._id}>
                            {product.name}
                          </option>
                        )
                      })}
                    </CFormSelect>
                  </CCol>
                  <CCol xs={2}>
                    <CFormInput
                      placeholder="Quantity"
                      type={'number'}
                      value={newOrder.orderDetail[i]?.quantity}
                      onChange={(e) => {
                        changeQuantity(e, i)
                      }}
                    />
                  </CCol>
                  <CCol xs={2}>
                    <CFormInput
                      disabled
                      placeholder="Price"
                      value={`${newOrder.orderDetail[i]?.price}$`}
                    />
                  </CCol>
                  <CCol xs={1}>
                    <button className="btn btn-danger " type="button">
                      <CIcon icon={cilTrash} onClick={() => onBtnDeleteClick(i)} />
                    </button>
                  </CCol>
                </CRow>
              )
            })}
          </Stack>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleNew(false)}>
            Cancel
          </CButton>
          <CButton color="success" onClick={handleNewOrder}>
            Create
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default NewOrder
