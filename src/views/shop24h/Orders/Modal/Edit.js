import {
  CModal,
  CModalBody,
  CButton,
  CModalFooter,
  CCol,
  CRow,
  CFormSelect,
  CFormInput,
  CFormLabel,
  CFormTextarea,
} from '@coreui/react'
import React, { useEffect, useState } from 'react'
import propTypes from 'prop-types'
import { Stack } from '@mui/material'
import { toast } from 'react-toastify'
import CIcon from '@coreui/icons-react'
import { cilTrash } from '@coreui/icons'

const EditOrder = ({
  visibleEdit,
  setVisibleEdit,
  refreshPage,
  setRefreshPage,
  orderSelected,
  setOrderSelected,
}) => {
  EditOrder.propTypes = {
    visibleEdit: propTypes.bool,
    setVisibleEdit: propTypes.func,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
    orderSelected: propTypes.object,
    setOrderSelected: propTypes.func,
  }

  const [products, setProducts] = useState([])

  useEffect(() => {
    const getAllProducts = async () => {
      fetch('http://localhost:8000/products')
        .then((res) => res.json())
        .then((data) => setProducts(data))
        .catch((error) => {
          console.error(error.message)
        })
    }
    getAllProducts()
  }, [])

  const onBtnAddNewProduct = () => {
    let newOrderDetail = {
      id: '',
      name: '',
      amount: 1,
      price: 0,
    }
    setOrderSelected({
      ...orderSelected,
      orderDetail: [...orderSelected.orderDetail, newOrderDetail],
    })
  }

  const changeSelectProduct = (event, index) => {
    const productId = event.target.value
    orderSelected.orderDetail[index].id = productId
    orderSelected.orderDetail[index].name = getName(productId)
    orderSelected.orderDetail[index].price =
      getPrice(orderSelected.orderDetail[index].id) *
      parseInt(orderSelected.orderDetail[index].amount)
    setOrderSelected({ ...orderSelected, orderDetail: [...orderSelected.orderDetail] })
  }

  const getName = (paramId) => {
    let productSelected = []
    if (paramId) {
      productSelected = products.filter((product, index) => {
        return product._id === paramId
      })
    }
    return productSelected[0].name
  }

  const getPrice = (paramId) => {
    let productSelected = []
    if (paramId) {
      productSelected = products.filter((product, index) => {
        return product._id === paramId
      })
    }
    return productSelected[0].buyPrice - productSelected[0].promotionPrice
  }

  const changeQuantity = (event, index) => {
    if (event.target.value > 0 && orderSelected.orderDetail[index].id !== '') {
      orderSelected.orderDetail[index].amount = event.target.value
      orderSelected.orderDetail[index].price =
        getPrice(orderSelected.orderDetail[index].id) *
        parseInt(orderSelected.orderDetail[index].amount)
      setOrderSelected({ ...orderSelected, orderDetail: [...orderSelected.orderDetail] })
    }
  }

  //sum total price
  const sumTotal = (orderDetail) => {
    const sum = orderDetail.reduce((preValue, product) => {
      return preValue + product.price
    }, 0)
    return sum
  }

  useEffect(() => {
    if (orderSelected.orderDetail) {
      setOrderSelected({ ...orderSelected, cost: sumTotal(orderSelected.orderDetail) })
    }
  }, [orderSelected.orderDetail])

  const onBtnDeleteClick = (index) => {
    orderSelected.orderDetail.splice(index, 1)
    setOrderSelected({ ...orderSelected, orderDetail: [...orderSelected.orderDetail] })
  }

  const handleEditOrder = () => {
    console.log(orderSelected)
    if (validatedData()) {
      fetch(`http://localhost:8000/orders/${orderSelected._id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(orderSelected),
      })
        .then((response) => response.json())
        .then((data) => {
          setVisibleEdit(false)
          toast.success(`Order ${orderSelected._id} has been updated!`, {
            position: 'top-center',
            autoClose: 500,
          })
          console.log(data)
          setRefreshPage(refreshPage + 1)
        })
        .catch((error) => {
          console.error('Error:', error.message)
        })
    }
  }

  //validation data
  const validatedData = () => {
    if (!orderSelected.orderDate) {
      toast.error('Order Date must be selected!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (!orderSelected.shippedDate) {
      toast.error('Shipped Date must be selected!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (!orderSelected.orderDetail) {
      toast.error('Products must be selected!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }
    return true
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visibleEdit}
        onClose={() => setVisibleEdit(false)}
      >
        <CModalBody>
          <h5 className="text-center">Order Information</h5>
          <Stack direction={'row'} spacing={1}>
            <div className="col-sm-12 col-md-6">
              <CRow className="mt-3">
                <CCol xs={4}>
                  <CFormLabel style={{ fontWeight: 'bolder' }}>Order Date</CFormLabel>
                </CCol>
                <CCol xs={8}>
                  <CFormInput
                    value={orderSelected.orderDate?.split('T')[0]}
                    type={'date'}
                    onChange={(e) =>
                      setOrderSelected({ ...orderSelected, orderDate: e.target.value })
                    }
                  />
                </CCol>
              </CRow>
            </div>
            <div className="col-sm-12 col-md-6">
              <CRow className="mt-3">
                <CCol xs={4}>
                  <CFormLabel style={{ fontWeight: 'bolder' }}>Shipped Date</CFormLabel>
                </CCol>
                <CCol xs={8}>
                  <CFormInput
                    value={orderSelected.shippedDate?.split('T')[0]}
                    type={'date'}
                    onChange={(e) =>
                      setOrderSelected({ ...orderSelected, shippedDate: e.target.value })
                    }
                  />
                </CCol>
              </CRow>
            </div>
          </Stack>
          <hr />
          <h5 className="text-center">Order Detail</h5>
          <Stack spacing={1}>
            <CRow>
              <CCol xs={8} className="text-left">
                <CButton onClick={onBtnAddNewProduct}>Add Product</CButton>
              </CCol>
              <CCol xs={4}>
                <CRow>
                  <CCol xs={4} className="text-right">
                    <CFormLabel style={{ fontWeight: 'bolder' }}>Total</CFormLabel>
                  </CCol>
                  <CCol xs={8}>
                    <CFormInput value={`${orderSelected.cost}$`} readOnly />
                  </CCol>
                </CRow>
              </CCol>
            </CRow>
            {orderSelected.orderDetail?.map((product, index) => {
              return (
                <CRow key={index} className="mt-2">
                  <CCol xs={7}>
                    <CFormSelect
                      value={orderSelected.orderDetail[index].id}
                      onChange={(e) => {
                        changeSelectProduct(e, index)
                      }}
                    >
                      <option value={'1'}>Select Product</option>
                      {products.map((product, index) => {
                        return (
                          <option key={index} value={product._id}>
                            {product.name}
                          </option>
                        )
                      })}
                    </CFormSelect>
                  </CCol>
                  <CCol xs={2}>
                    <CFormInput
                      placeholder="Quantity"
                      type={'number'}
                      value={orderSelected.orderDetail[index].amount}
                      onChange={(e) => {
                        changeQuantity(e, index)
                      }}
                    />
                  </CCol>
                  <CCol xs={2}>
                    <CFormInput
                      disabled
                      placeholder="Price"
                      value={`${orderSelected.orderDetail[index].price}$`}
                    />
                  </CCol>
                  <CCol xs={1}>
                    <button className="btn btn-danger " type="button">
                      <CIcon icon={cilTrash} onClick={() => onBtnDeleteClick(index)} />
                    </button>
                  </CCol>
                </CRow>
              )
            })}
          </Stack>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleEdit(false)}>
            Cancel
          </CButton>
          <CButton color="primary" onClick={handleEditOrder}>
            Save Changes
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default EditOrder
