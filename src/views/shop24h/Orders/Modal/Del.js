import { CModal, CModalBody, CModalHeader, CModalTitle, CButton, CModalFooter } from '@coreui/react'
import React from 'react'
import propTypes from 'prop-types'
import { toast } from 'react-toastify'

const DelOrder = ({
  visibleDel,
  setVisibleDel,
  orderSelected,
  refreshPage,
  setRefreshPage,
}) => {
  DelOrder.propTypes = {
    visibleDel: propTypes.bool,
    setVisibleDel: propTypes.func,
    orderSelected: propTypes.object,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
  }

  const handleDelOrder = () => {
    fetch(`http://localhost:8000/orders/${orderSelected._id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((data) => {
        toast.warning('The order has been deleted!', {
          position: 'top-center',
          autoClose: 500,
        })
        setVisibleDel(false)
        setRefreshPage(refreshPage + 1)
      })
      .catch((error) => {
        console.error('Error:', error.message)
      })
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visibleDel}
        onClose={() => setVisibleDel(false)}
      >
        <CModalHeader color="danger" onClose={() => setVisibleDel(false)}>
          <CModalTitle>Delete Confirmation</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <h5 className="text-center text-danger">Are you sure to DELETE order <strong>{orderSelected._id}</strong>???</h5>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleDel(false)}>
            Cancel
          </CButton>
          <CButton color="danger" onClick={handleDelOrder}>
            Confirm
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default DelOrder
