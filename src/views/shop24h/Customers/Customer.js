import React, { useEffect, useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CFormInput,
  CButton,
} from '@coreui/react'
import { Stack, Pagination, IconButton } from '@mui/material'
import { AddCircleRounded } from '@mui/icons-material'
import NewCustomer from './Modal/New'
import EditCustomer from './Modal/Edit'
import DelCustomer from './Modal/Del'
import OrderDetail from './Modal/OrderDetail'

const Customers = () => {
  const [customers, setCustomers] = useState([])
  const [noPage, setNoPage] = useState(5)
  const [pageIndex, setPageIndex] = useState(1)
  const [refreshPage, setRefreshPage] = useState(0)
  const [searchInfo, setSearchInfo] = useState('')

  const onPageIndexChange = (event, value) => {
    setPageIndex(value)
  }

  const getAllCustomers = async () => {
    fetch('http://localhost:8000/customers')
      .then((res) => res.json())
      .then((data) => {
        setNoPage(Math.ceil(data.length / 8))
        setCustomers(data.slice((pageIndex - 1) * 8, pageIndex * 8))
      })
      .catch((error) => {
        console.error(error.message)
      })
  }

  useEffect(() => {
    getAllCustomers()
  }, [refreshPage, noPage, pageIndex])

  //filter
  const handleSearchByPhone = (event) => {
    if (event.key === 'Enter') {
      const findCustomer = async () => {
        fetch('http://localhost:8000/customers')
          .then((res) => res.json())
          .then((data) => {
            console.log(searchInfo)
            const updateCustomerList = data.filter((p) => p.phone === searchInfo)
            console.log(updateCustomerList)
            setCustomers(updateCustomerList)
          })
          .catch((error) => {
            console.log(error)
          })
      }
      findCustomer()
    }
  }

  //view order detail
  const [visibleOrder, setVisibleOrder] = useState(false)
  const [orderDetail, setOrderDetail] = useState([])
  const handleOrderDetail = (customer) => {
    setVisibleOrder(true);
    setOrderDetail(customer)
  }

  //clear filter
  const handleClearBtn = () => {
    setSearchInfo('')
    getAllCustomers()
  }

  //add new
  const [visibleNew, setVisibleNew] = useState(false)
  const handleAddNewCustomer = () => {
    setVisibleNew(true)
  }

  //update
  const [visibleEdit, setVisibleEdit] = useState(false)
  const [customerSelected, setCustomerSelected] = useState({})
  const handleEditCustomer = (customer) => {
    setVisibleEdit(true)
    setCustomerSelected(customer)
  }

  //delete
  const [visibleDel, setVisibleDel] = useState(false)
  const handleDelCustomer = (customer) => {
    setVisibleDel(true)
    setCustomerSelected(customer)
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Customers</strong>
          </CCardHeader>
          <CCardBody>
            <div className="d-flex justify-content-between mb-2">
              <div className="col-2">
                <IconButton
                  color="success"
                  aria-label="add new product"
                  size="small"
                  onClick={handleAddNewCustomer}
                >
                  <AddCircleRounded />
                  New Customer
                </IconButton>
              </div>
              <div className="row col-4">
                <div className="col-8">
                  <CFormInput
                    placeholder="search by phone.."
                    value={searchInfo}
                    onChange={(e) => setSearchInfo(e.target.value)}
                    onKeyDown={handleSearchByPhone}
                  ></CFormInput>
                </div>
                <div className="col-4" style={{ marginLeft: '-18px' }}>
                  <CButton color="danger" onClick={handleClearBtn}>
                    Clear
                  </CButton>
                </div>
              </div>
            </div>
            <CTable color="success" striped hover responsive>
              <CTableHead>
                <CTableRow>
                  <CTableHeaderCell scope="col">ID</CTableHeaderCell>
                  <CTableHeaderCell scope="col">FullName</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Phone</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Email</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Address</CTableHeaderCell>
                  <CTableHeaderCell scope="col">City</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Country</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Orders</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                </CTableRow>
              </CTableHead>
              <CTableBody>
                {customers.map((customer, index) => {
                  return (
                    <CTableRow key={customer.email}>
                      <CTableHeaderCell>{customer._id}</CTableHeaderCell>
                      <CTableDataCell>{customer.fullName}</CTableDataCell>
                      <CTableDataCell>{customer.phone}</CTableDataCell>
                      <CTableDataCell>{customer.email}</CTableDataCell>
                      <CTableDataCell>{customer.address}</CTableDataCell>
                      <CTableDataCell>{customer.city}</CTableDataCell>
                      <CTableDataCell>{customer.country}</CTableDataCell>
                      <CTableDataCell>
                        {customer.orders.length > 0 ? (
                          <button
                            className="btn btn-secondary"
                            onClick={() => handleOrderDetail(customer)}
                          >
                            Order Detail
                          </button>
                        ) : (
                          0
                        )}
                      </CTableDataCell>
                      <CTableDataCell className="row">
                        <button
                          className="btn btn-primary mx-1"
                          style={{ width: '60px' }}
                          onClick={() => handleEditCustomer(customer)}
                        >
                          Edit
                        </button>
                        <button
                          className="btn btn-danger"
                          style={{ width: '60px' }}
                          onClick={() => handleDelCustomer(customer)}
                        >
                          Del
                        </button>
                      </CTableDataCell>
                    </CTableRow>
                  )
                })}
              </CTableBody>
            </CTable>
            <Stack spacing={2} mt={2} mb={1} direction="row" justifyContent={'flex-end'}>
              <Pagination
                variant="outlined"
                color="secondary"
                defaultPage={1}
                count={noPage}
                onChange={onPageIndexChange}
              />
            </Stack>
          </CCardBody>
        </CCard>
        {/* Modal */}
        <NewCustomer
          visibleNew={visibleNew}
          setVisibleNew={setVisibleNew}
          refreshPage={refreshPage}
          setRefreshPage={setRefreshPage}
        />
        <EditCustomer
          visibleEdit={visibleEdit}
          setVisibleEdit={setVisibleEdit}
          refreshPage={refreshPage}
          setRefreshPage={setRefreshPage}
          customerSelected={customerSelected}
          setCustomerSelected={setCustomerSelected}
        />
        <DelCustomer
          visibleDel={visibleDel}
          setVisibleDel={setVisibleDel}
          refreshPage={refreshPage}
          setRefreshPage={setRefreshPage}
          customerSelected={customerSelected}
        />
        <OrderDetail
          visibleOrder={visibleOrder}
          setVisibleOrder={setVisibleOrder}
          orderDetail={orderDetail}
        />
      </CCol>
    </CRow>
  )
}

export default Customers
