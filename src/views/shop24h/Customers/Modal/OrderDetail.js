import {
  CModal,
  CModalBody,
  CModalHeader,
  CModalTitle,
  CButton,
  CModalFooter,
  CTable,
  CTableRow,
  CTableHead,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
} from '@coreui/react'
import React from 'react'
import propTypes from 'prop-types'

const OrderDetail = ({ visibleOrder, setVisibleOrder, orderDetail }) => {
  OrderDetail.propTypes = {
    visibleOrder: propTypes.bool,
    setVisibleOrder: propTypes.func,
    orderDetail: propTypes.array,
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visibleOrder}
        onClose={() => setVisibleOrder(false)}
      >
        <CModalHeader onClose={() => setVisibleOrder(false)}>
          <CModalTitle>Order Detail</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CTable color="info" striped hover responsive>
            <CTableHead>
              <CTableRow style={{ padding: '0 10px' }}>
                <CTableHeaderCell>ID</CTableHeaderCell>
                <CTableHeaderCell>Order Details</CTableHeaderCell>
                <CTableHeaderCell>Total Cost</CTableHeaderCell>
                <CTableHeaderCell>Order Date</CTableHeaderCell>
                <CTableHeaderCell>Shipped Date</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {orderDetail.orders?.map((ord, index) => {
                return (
                  <CTableRow key={index}>
                    <CTableHeaderCell>{ord._id}</CTableHeaderCell>
                    <CTableDataCell>
                      {ord.orderDetail.map((product) => (
                        <p key={product._id}>
                          {product.name} x{product.amount}
                        </p>
                      ))}
                    </CTableDataCell>
                    <CTableDataCell>{ord.cost}$</CTableDataCell>
                    <CTableDataCell>{ord.orderDate.slice(0, 10)}</CTableDataCell>
                    <CTableDataCell>{ord.shippedDate.slice(0, 10)}</CTableDataCell>
                  </CTableRow>
                )
              })}
            </CTableBody>
          </CTable>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleOrder(false)}>
            Cancel
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default OrderDetail
