import { CModal, CModalBody, CModalHeader, CModalTitle, CButton, CModalFooter } from '@coreui/react'
import React, { useState } from 'react'
import propTypes from 'prop-types'
import { Stack, TextField } from '@mui/material'
import { toast } from 'react-toastify'

const NewCustomer = ({ visibleNew, setVisibleNew, refreshPage, setRefreshPage }) => {
  NewCustomer.propTypes = {
    visibleNew: propTypes.bool,
    setVisibleNew: propTypes.func,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
  }

  const [newCustomer, setNewCustomer] = useState({
    fullName: '',
    phone: '',
    email: '',
    address: '',
    city: '',
    country: '',
  })

  const handleNewCustomer = () => {
    if (validatedData()) {
      fetch(`http://localhost:8000/customers/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newCustomer),
      })
        .then((response) => response.json())
        .then((data) => {
          toast.success('New Customer has been created!', {
            position: 'top-center',
            autoClose: 500,
          })
          console.log(data)
          setVisibleNew(false)
          setNewCustomer({
            fullName: '',
            phone: '',
            email: '',
            address: '',
            city: '',
            country: '',
          })
          setRefreshPage(refreshPage + 1)
        })
        .catch((error) => {
          console.error('Error:', error.message)
        })
    }
  }

  //validation data
  const validatedData = () => {
    if (newCustomer.fullName === '') {
      toast.error(`Customer's name must be filled!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newCustomer.phone === '') {
      toast.error(`Customer's phone must be filled!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newCustomer.phone.length > 15) {
      toast.error(`Customer's phone invalid!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newCustomer.email === '') {
      toast.error(`Customer's email must be filled!!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(newCustomer.email)) {
      toast.error(`Customer's email invalid!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newCustomer.address === '') {
      toast.error(`Customer's address must be filled!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newCustomer.city === '') {
      toast.error(`Customer's city must be filled!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newCustomer.country === '') {
      toast.error(`Customer's country must be filled!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    return true
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visibleNew}
        onClose={() => setVisibleNew(false)}
      >
        <CModalHeader onClose={() => setVisibleNew(false)}>
          <CModalTitle>New Customer</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Stack spacing={2}>
            <div>
              <TextField
                id="name"
                value={newCustomer.fullName}
                variant="filled"
                label="Full Name"
                fullWidth
                onChange={(e) => setNewCustomer({ ...newCustomer, fullName: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="phone"
                value={newCustomer.phone}
                variant="filled"
                label="Phone Number"
                fullWidth
                onChange={(e) => setNewCustomer({ ...newCustomer, phone: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="email"
                value={newCustomer.email}
                variant="filled"
                label="Email"
                fullWidth
                onChange={(e) => setNewCustomer({ ...newCustomer, email: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="address"
                value={newCustomer.address}
                variant="filled"
                label="Address"
                fullWidth
                onChange={(e) => setNewCustomer({ ...newCustomer, address: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="city"
                value={newCustomer.city}
                variant="filled"
                label="City"
                fullWidth
                onChange={(e) => setNewCustomer({ ...newCustomer, city: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="country"
                value={newCustomer.country}
                variant="filled"
                label="Country"
                fullWidth
                onChange={(e) => setNewCustomer({ ...newCustomer, country: e.target.value })}
              />
            </div>
          </Stack>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleNew(false)}>
            Cancel
          </CButton>
          <CButton color="success" onClick={handleNewCustomer}>
            Create
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default NewCustomer
