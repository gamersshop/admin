import { CModal, CModalBody, CModalHeader, CModalTitle, CButton, CModalFooter } from '@coreui/react'
import React from 'react'
import propTypes from 'prop-types'
import { Stack, TextField } from '@mui/material'
import { toast } from 'react-toastify'

const EditCustomer = ({
  visibleEdit,
  setVisibleEdit,
  refreshPage,
  setRefreshPage,
  customerSelected,
  setCustomerSelected,
}) => {
  EditCustomer.propTypes = {
    visibleEdit: propTypes.bool,
    setVisibleEdit: propTypes.func,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
    customerSelected: propTypes.object,
    setCustomerSelected: propTypes.func,
  }

  const handleEditCustomer = () => {
    if (validatedData()) {
      fetch(`http://localhost:8000/customers/${customerSelected._id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(customerSelected),
      })
        .then((response) => response.json())
        .then((data) => {
          toast.success('The customer has been updated!', {
            position: 'top-center',
            autoClose: 500,
          })
          console.log(data)
          setVisibleEdit(false)
          setRefreshPage(refreshPage + 1)
        })
        .catch((error) => {
          toast.error('Update failed!', {
            position: 'top-center',
            autoClose: 500,
          })
          console.error('Error:', error.message)
        })
    }
  }

  //validation data
  const validatedData = () => {
    if (customerSelected.fullName === '') {
      toast.error(`Customer's name must be filled!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (customerSelected.phone === '') {
      toast.error(`Customer's phone must be filled!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (customerSelected.phone.length > 15) {
      toast.error(`Customer's phone invalid!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (customerSelected.email > 15) {
      toast.error(`Customer's email must be filled!!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(customerSelected.email)) {
      toast.error(`Customer's email invalid!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (customerSelected.address === '') {
      toast.error(`Customer's address must be filled!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (customerSelected.city === '') {
      toast.error(`Customer's city must be filled!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (customerSelected.country === '') {
      toast.error(`Customer's country must be filled!`, {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    return true
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visibleEdit}
        onClose={() => setVisibleEdit(false)}
      >
        <CModalHeader onClose={() => setVisibleEdit(false)}>
          <CModalTitle>Update Customer</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Stack spacing={2}>
            <div>
              <TextField
                id="name"
                value={customerSelected.fullName}
                variant="filled"
                label="Full Name"
                fullWidth
                onChange={(e) => setCustomerSelected({ ...customerSelected, fullName: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="phone"
                value={customerSelected.phone}
                variant="filled"
                label="Phone Number"
                fullWidth
                onChange={(e) => setCustomerSelected({ ...customerSelected, phone: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="email"
                value={customerSelected.email}
                variant="filled"
                label="Email"
                fullWidth
                onChange={(e) => setCustomerSelected({ ...customerSelected, email: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="address"
                value={customerSelected.address}
                variant="filled"
                label="Address"
                fullWidth
                onChange={(e) => setCustomerSelected({ ...customerSelected, address: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="city"
                value={customerSelected.city}
                variant="filled"
                label="City"
                fullWidth
                onChange={(e) => setCustomerSelected({ ...customerSelected, city: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="country"
                value={customerSelected.country}
                variant="filled"
                label="Country"
                fullWidth
                onChange={(e) => setCustomerSelected({ ...customerSelected, country: e.target.value })}
              />
            </div>
          </Stack>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleEdit(false)}>
            Cancel
          </CButton>
          <CButton color="primary" onClick={handleEditCustomer}>
            Save changes
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default EditCustomer
