import React from 'react'
import {
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
  CRow,
  CCol,
  CCard,
  CCardHeader,
  CCardBody,
} from '@coreui/react'
import { IconButton } from '@mui/material'
import { AddCircleRounded } from '@mui/icons-material'
import { useEffect, useState } from 'react'
import NewPlatform from './Modal/New'
import EditPlatform from './Modal/Edit'
import DelPlatform from './Modal/Del'

function Platform() {
  const fetchAPI = async (url, body) => {
    let response = await fetch(url, body)
    let data = await response.json()
    return data
  }

  const [platform, setPlatform] = useState([])
  const [refreshPage, setRefreshPage] = useState(0)

  useEffect(() => {
    fetchAPI('http://localhost:8000/platforms')
      .then((data) => {
        setPlatform(data)
        console.log(data)
      })
      .catch((error) => {
        console.log(error)
      })
  }, [refreshPage])

  //add new
  const [visible, setVisible] = useState(false)
  const handleAddNewPlatformBtn = () => {
    setVisible(true)
  }

  //update
  const [visibleEdit, setVisibleEdit] = useState(false)
  const [platformSelected, setPlatformSelected] = useState({})
  const handleEditPlatformBtn = (type) => {
    setVisibleEdit(true)
    setPlatformSelected(type)
  }

  //del
  const [visibleDel, setVisibleDel] = useState(false)
  const handleDelPlatformBtn = (type) => {
    setVisibleDel(true)
    setPlatformSelected(type)
  }

  return (
    <>
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>
              <strong>Platform</strong>
            </CCardHeader>
            <CCardBody>
            <div className="col-2">
                <IconButton
                  color="success"
                  aria-label="add new platform"
                  size="small"
                  onClick={handleAddNewPlatformBtn}
                >
                  <AddCircleRounded />
                  New Platform
                </IconButton>
              </div>
              <CTable color="info" striped hover responsive>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">ID</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Description</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Action</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {platform.map((platform, index) => {
                    return (
                      <CTableRow key={platform._id}>
                        <CTableDataCell>{platform._id}</CTableDataCell>
                        <CTableDataCell>{platform.name}</CTableDataCell>
                        <CTableDataCell>{platform.description}</CTableDataCell>
                        <CTableDataCell className="col">
                          <button
                            className="btn btn-primary mb-2"
                            style={{ width: '80px' }}
                            onClick={() => handleEditPlatformBtn(platform)}
                          >
                            Edit
                          </button>
                          <button
                            className="btn btn-danger"
                            style={{ width: '80px' }}
                            onClick={() => handleDelPlatformBtn(platform)}
                          >
                            Del
                          </button>
                        </CTableDataCell>
                      </CTableRow>
                    )
                  })}
                </CTableBody>
              </CTable>
            </CCardBody>
          </CCard>
          {/* Modal */}
          <NewPlatform
            visible={visible}
            setVisible={setVisible}
            refreshPage={refreshPage}
            setRefreshPage={setRefreshPage}
          />
          <EditPlatform
            visibleEdit={visibleEdit}
            setVisibleEdit={setVisibleEdit}
            refreshPage={refreshPage}
            setRefreshPage={setRefreshPage}
            platformSelected={platformSelected} 
            setPlatformSelected={setPlatformSelected}    
          />
          <DelPlatform
            visibleDel={visibleDel}
            setVisibleDel={setVisibleDel}
            refreshPage={refreshPage}
            setRefreshPage={setRefreshPage}
            platformSelected={platformSelected} 
          />
        </CCol>
      </CRow>
    </>
  )
}

export default Platform
