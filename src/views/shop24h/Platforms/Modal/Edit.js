import { CModal, CModalBody, CModalHeader, CModalTitle, CButton, CModalFooter } from '@coreui/react'
import React, { useState } from 'react'
import propTypes from 'prop-types'
import { Stack, TextField } from '@mui/material'
import { toast } from 'react-toastify'

const EditPlatform = ({ visibleEdit, setVisibleEdit, refreshPage, setRefreshPage, platformSelected, setPlatformSelected }) => {
  EditPlatform.propTypes = {
    visibleEdit: propTypes.bool,
    setVisibleEdit: propTypes.func,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
    platformSelected: propTypes.object,
    setPlatformSelected: propTypes.func
  }

  const handleEditPlatform = () => {
    if (validatedData()) {
      fetch(`http://localhost:8000/platforms/${platformSelected._id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(platformSelected),
      })
        .then((response) => response.json())
        .then((data) => {
          toast.success('Your platform has been updated!', {
            position: 'top-center',
            autoClose: 500,
          })
          console.log(data)
          setVisibleEdit(false)
          setRefreshPage(refreshPage + 1)
        })
        .catch((error) => {
          console.error('Error:', error.message)
        })
    }
  }

  //validation data
  const validatedData = () => {
    if (platformSelected.name === '') {
      toast.error('Your platform name must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (platformSelected.description === '') {
      toast.error('Your platform description must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    return true
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visibleEdit}
        onClose={() => setVisibleEdit(false)}
      >
        <CModalHeader onClose={() => setVisibleEdit(false)}>
          <CModalTitle>Update Platform</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Stack spacing={2}>
            <div>
              <TextField
                id="name"
                value={platformSelected.name}
                variant="filled"
                label="Name"
                fullWidth
                onChange={(e) => setPlatformSelected({ ...platformSelected, name: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="description"
                value={platformSelected.description}
                multiline
                rows={3}
                variant="filled"
                label="Description"
                fullWidth
                onChange={(e) => setPlatformSelected({ ...platformSelected, description: e.target.value })}
              />
            </div>
          </Stack>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleEdit(false)}>
            Cancel
          </CButton>
          <CButton color="primary" onClick={handleEditPlatform}>
            Save changes
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default EditPlatform
