import { CModal, CModalBody, CModalHeader, CModalTitle, CButton, CModalFooter } from '@coreui/react'
import React, { useState } from 'react'
import propTypes from 'prop-types'
import { Stack, TextField } from '@mui/material'
import { toast } from 'react-toastify'

const NewPlatform = ({ visible, setVisible, refreshPage, setRefreshPage }) => {
  NewPlatform.propTypes = {
    visible: propTypes.bool,
    setVisible: propTypes.func,
    refreshPage: propTypes.number,
    setRefreshPage: propTypes.func,
  }

  const [newPlatform, setNewPlatform] = useState({
    name: '',
    description: '',
  })

  const handleNewPlatform = () => {
    if (validatedData()) {
      fetch(`http://localhost:8000/platforms/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newPlatform),
      })
        .then((response) => response.json())
        .then((data) => {
          toast.success('Your platform has been created!', {
            position: 'top-center',
            autoClose: 500,
          })
          console.log(data)
          setVisible(false)
          setNewPlatform({
            name: '',
            description: '',
          })
          setRefreshPage(refreshPage + 1)
        })
        .catch((error) => {
          console.error('Error:', error.message)
        })
    }
  }

  //validation data
  const validatedData = () => {
    if (newPlatform.name === '') {
      toast.error('Your platform name must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    if (newPlatform.description === '') {
      toast.error('Your platform description must be filled!', {
        position: 'top-center',
        autoClose: 500,
      })
      return false
    }

    return true
  }

  return (
    <>
      <CModal
        alignment="center"
        scrollable
        size="lg"
        visible={visible}
        onClose={() => setVisible(false)}
      >
        <CModalHeader onClose={() => setVisible(false)}>
          <CModalTitle>New Platform</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <Stack spacing={2}>
            <div>
              <TextField
                id="name"
                value={newPlatform.name}
                variant="filled"
                label="Name"
                fullWidth
                onChange={(e) => setNewPlatform({ ...newPlatform, name: e.target.value })}
              />
            </div>
            <div>
              <TextField
                id="description"
                value={newPlatform.description}
                multiline
                rows={3}
                variant="filled"
                label="Description"
                fullWidth
                onChange={(e) => setNewPlatform({ ...newPlatform, description: e.target.value })}
              />
            </div>
          </Stack>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisible(false)}>
            Cancel
          </CButton>
          <CButton color="success" onClick={handleNewPlatform}>
            Save changes
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default NewPlatform
