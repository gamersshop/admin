import React from 'react'
import CIcon from '@coreui/icons-react'
import { cilGamepad, cilUser, cilMenu } from '@coreui/icons'

import { CNavGroup, CNavItem } from '@coreui/react'

const _nav = [
  {
    component: CNavGroup,
    name: 'Shop24h',
    to: '/shop24h',
    icon: <CIcon icon={cilGamepad} customClassName="nav-icon" />,
    badge: {
      color: 'info',
      text: 'NEW',
    },
    items: [
      {
        component: CNavItem,
        name: 'Products',
        to: '/shop24h/products',
      },
      {
        component: CNavItem,
        name: 'Categories',
        to: '/shop24h/categories',
      },
      {
        component: CNavItem,
        name: 'Platforms',
        to: '/shop24h/platforms',
      },
      {
        component: CNavItem,
        icon: <CIcon icon={cilUser} customClassName="nav-icon" />,

        name: 'Customers',
        to: '/shop24h/customers',
      },
      {
        component: CNavItem,
        icon: <CIcon icon={cilMenu} customClassName="nav-icon" />,

        name: 'Orders',
        to: '/shop24h/orders',
      },
    ],
  },
]

export default _nav
