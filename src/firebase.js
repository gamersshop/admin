import { initializeApp } from 'firebase/app'
import { getStorage } from 'firebase/storage'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyCS4s0D9qcefiOsYZ8ucnAbiBGHwQxbfKg',
  authDomain: 'shop24h-devcamp.firebaseapp.com',
  projectId: 'shop24h-devcamp',
  storageBucket: 'shop24h-devcamp.appspot.com',
  messagingSenderId: '354029096619',
  appId: '1:354029096619:web:f47f1791b8a3778dee31ee',
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)
export const storage = getStorage(app)
